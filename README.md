# Elf ICDE Version
This repository represents the version of the Elf presented on the ICDE. Data file not added.. please add.
https://git.iti.cs.ovgu.de/dbronesk/ICDE-elf
## Dependencies
- gcc or clang with c++11 support
- cmake >= 3.2
- Boost >= 1.63

## How to build
1. Open a shell in the project root folder
2. type `./configure.sh` and hit enter
3. navigate to the newly created build folder
4. use `make` and wait until the buildprocess has finished
5. The Elf_cleaned binary has been build