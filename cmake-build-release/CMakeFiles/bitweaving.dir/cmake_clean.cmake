file(REMOVE_RECURSE
  "CMakeFiles/bitweaving"
  "CMakeFiles/bitweaving-complete"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-install"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-mkdir"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-download"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-update"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-patch"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-configure"
  "bitweaving-prefix/src/bitweaving-stamp/bitweaving-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/bitweaving.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
